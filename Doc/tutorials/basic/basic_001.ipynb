{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial can be downloaded [link](http://greatfire.uchicago.edu/west-public/West/raw/master/Doc/tutorials/basic/basic_001.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Intro tutorial 1: Getting Started with full-frequency GW calculations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to compute the full-frequency $G_0W_0$ electronic structure of the silane molecule you need to run `pw.x`, `wstat.x` and `wfreq.x` in sequence. Documentation for building and installing WEST is available at this [link](http://www.west-code.org/doc/West/latest/installation.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The GW workflow involves three sequental steps:\n",
    "\n",
    "- Step 1: Mean-field starting point\n",
    "- Step 2: Calculation of dielectric screening\n",
    "- Step 3: Calculation of quasiparticle corrections\n",
    "\n",
    "Each step is explained below. At the end of step 3 you will be able to obtain the electronic structure of the silane molecule at the $G_0W_0 @ PBE$ level of theory, where the GW is computed without empty states and with full frequency integration using the countour deformation technique. For more information about the implementation, we refer to [Govoni et al., J. Chem. Theory Comput. 11, 2680 (2015)](https://doi.org/10.1021/ct500958p)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 1: Mean-field starting point"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The mean-field electronic structure of the silane molecule is obtained using density functional theory (DFT), as implemented in the [Quantum ESPRESSO](https://www.quantum-espresso.org/) code. This is obtained by running `pw.x`. Check the installation section of the documentation to understand which version of Quantum ESPRESSO is compatible with WEST. The ONCV pseudopotential files for **Si** and **H** in UPF format (obtained using the PBE exchange-correlation functional) can be downloaded from: [QE-PP](https://www.quantum-espresso.org/pseudopotentials) database, or from [SG15](http://www.quantum-simulation.org/potentials/sg15_oncv/upf/) database. Check out the `pw.x` [input description](https://www.quantum-espresso.org/Doc/INPUT_PW.html) in order to generate an input file for Quantum ESPRESSO called `pw.in`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Download the following files in your current working directory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "wget -N -q http://www.west-code.org/doc/training/silane/pw.in\n",
    "wget -N -q http://www.quantum-simulation.org/potentials/sg15_oncv/upf/H_ONCV_PBE-1.2.upf\n",
    "wget -N -q http://www.quantum-simulation.org/potentials/sg15_oncv/upf/Si_ONCV_PBE-1.2.upf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's inspect the ``pw.in`` file, input for ``pw.x``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "&control\n",
      "calculation  = 'scf'\n",
      "restart_mode = 'from_scratch'\n",
      "pseudo_dir   = './'\n",
      "outdir       = './'\n",
      "prefix       = 'silane'\n",
      "wf_collect   = .TRUE.\n",
      "/\n",
      "&system\n",
      "ibrav           = 1\n",
      "celldm(1)       = 20\n",
      "nat             = 5\n",
      "ntyp            = 2\n",
      "ecutwfc         = 25.0\n",
      "nbnd            = 10\n",
      "assume_isolated ='mp'\n",
      "/\n",
      "&electrons\n",
      "diago_full_acc = .TRUE.\n",
      "/\n",
      "ATOMIC_SPECIES\n",
      "Si 28.0855  Si_ONCV_PBE-1.2.upf\n",
      "H  1.00794   H_ONCV_PBE-1.2.upf\n",
      "ATOMIC_POSITIONS bohr\n",
      "Si      10.000000   10.000000  10.000000\n",
      "H       11.614581   11.614581  11.614581\n",
      "H        8.385418    8.385418  11.614581\n",
      "H        8.385418   11.614581   8.385418\n",
      "H       11.614581    8.385418   8.385418\n",
      "K_POINTS {gamma}\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cat pw.in"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a molecule we sampled the Brillouin zone using the Gamma point only. For periodic systems you can use an automatic and centered k-points mesh. We have placed the molecule in a cubic cell with an edge of 20 a.u. (`celldm(1)`). The size of the basis set (how many Fourier components are used to describe single-particle wavefunctions) is specified by setting the variable `ecutwfc` to 25 Ry. All these parameters are system dependent and may need to be converged."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run ``pw.x`` on 2 cores. The number of cores may be increased depending on the size of the system and the available computational resources."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "mpirun -n 2 pw.x -i pw.in > pw.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output file ``pw.out`` contains information about the mean-field calculation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 2: Calculation of dielectric screening"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The static dielectric screening is computed using the projective dielectric eigendecomposition (PDEP) technique. Check out the ``wstat.x`` [input description](http://www.west-code.org/doc/West/latest/) and generate an input file for WEST called ``wstat.in``."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Download this file in your current working directory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "wget -N -q http://www.west-code.org/doc/training/silane/wstat.in"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's inspect the ``wstat.in`` file, input for ``wstat.x``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "input_west:\n",
      "    qe_prefix: silane\n",
      "    west_prefix: silane\n",
      "    outdir: ./\n",
      "\n",
      "wstat_control:\n",
      "    wstat_calculation: S\n",
      "    n_pdep_eigen: 50"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cat wstat.in"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this input file we compute 50 PDEPs, i.e., 50 eigenvectors of the static dielectric matrix. This step uses the *occupied* single-particle states and energies obtained in the previous step (mean-field calculation). Note that **unoccupied states are not needed**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run ``wstat.x`` on 2 cores. The number of cores may be increased depending on the size of the system and the available computational resources. [Tutorial 4](http://greatfire.uchicago.edu/west-public/West/raw/master/Doc/tutorials/basic/basic_004.ipynb) explains additional flags that can be used to control the distribution of data-structures and loops. In this tutorial, we are not going to use any specific parallelization flag, which means that Fourier transform operations are distributed using the chosen number of cores (2)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "mpirun -n 2 wstat.x -i wstat.in > wstat.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output file ``wstat.out`` contains information about the PDEP iterations. Dielectric eigenvalues can be found in the file ``<west_prefix>.wstat.save/wstat.json``. This file is machine readable (JSON format)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the reader does NOT have the computational resources to run the calculation, the WEST output file needed for the next step can be directly downloaded as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "mkdir -p silane.wstat.save\n",
    "wget -N -q http://www.west-code.org/doc/training/silane/wstat.json -O silane.wstat.save/wstat.json"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we show how to load, print, and plot the PDEP eigenvalues."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "import numpy as np\n",
    "\n",
    "# Load the output data\n",
    "with open('silane.wstat.save/wstat.json') as json_file :\n",
    "    data = json.load(json_file)\n",
    "\n",
    "# Extract converged PDEP eigenvalues\n",
    "ev = np.array(data['exec']['davitr'][-1]['ev'],dtype='f8') # -1 identifies the last iteration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[-1.2746729  -1.19123668 -1.19114095 -1.19109545 -0.82404822 -0.82400394\n",
      " -0.82390314 -0.635782   -0.62934203 -0.6293231  -0.50049559 -0.50047539\n",
      " -0.50041874 -0.42991703 -0.42990245 -0.42988114 -0.23238097 -0.2323769\n",
      " -0.23236338 -0.18323023 -0.18320714 -0.18319987 -0.17839625 -0.17749216\n",
      " -0.1774905  -0.14592151 -0.1459186  -0.14590667 -0.12256418 -0.12011843\n",
      " -0.12011402 -0.12010991 -0.11634223 -0.11634118 -0.11528637 -0.11528175\n",
      " -0.11528151 -0.09407982 -0.09407648 -0.09407436 -0.07995305 -0.07994979\n",
      " -0.0799484  -0.07477637 -0.07309848 -0.07309674 -0.06577741 -0.06577163\n",
      " -0.06576642 -0.06313331]\n"
     ]
    }
   ],
   "source": [
    "# Print\n",
    "print(ev)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYoAAAEGCAYAAAB7DNKzAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMSwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/YYfK9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAgN0lEQVR4nO3de5RddX338fd3LklObgy5ZwaSAIaRcI1MBY1LIaDBSCFFfZa0ttT6lPapfQpWg4l2qb1YqGldPj6Py7VSseZ5oK1UQqCCjBBAvFR0QjCTMRmDYsLMJDMTQmaSMDOZy/f54+yBuexz5kzm7LPP5fNaa9Y5+7f32ee7V2B/z++6zd0RERFJpSzuAEREJL8pUYiISFpKFCIikpYShYiIpKVEISIiaVXEHUAUFixY4CtWrIg7DBGRgrFr166j7r4wbF9RJooVK1bQ0NAQdxgiIgXDzA6m2qemJxERSUuJQkRE0lKiEBGRtJQoREQkLSUKERFJqyhHPYmIlJIdu1vZUt9M2/EeqqsSbFxXy4bVNVk7vxKFiEhMUt3gJ1MOsHl7Iz39gwC0Hu9h8/ZGgKwlCyvGZcbr6upc8yhEJArZuLkPl4+8wQPMqCzjty+r5j/3tNHbPzSq/MbLqvnOmPJp5WVUVhin+gYZq6YqwY82rc34usxsl7vXhe5TohCRYjfZm3iqzwDjbu6JynLef2UND+5qzah8WrlxTe1Cvv/Lo/QNvHHTzzYDXrrnfZkfr0QhIqUq7Jd7upv73bdcCoxPCNMryqgsN06G/HrPR9msUaiPQkTyVjaaebbUN4+64QP09A9y308Ojfu+nv5B7vr2HgBOD47+td83METfQHauy4DqqgStx3vG7Ss3YzDkB3yq8qpEJX0DQ+MS3nANKCvxqkYhIvlosjWBsPLKcqN/MPp73GRv7jVBEpvq9Y2sAU111JNqFCJScFLVBO7/ySHG3npTladLEulu4kDor/1Uv94ne3MfeSMPu8HXLZ83qXLI3ginMKpRiEjeGRxyLvj0Y1k7X6KyfMp9FBP9ej+TDvN8os5sETlj2RwOmkn5jZcv5Zn9nTS3nwiNp8xgKOS2laq8ZkRfxVRHPeXjDT5blChEilQubuLZaEefTDnAvFmV3HR5Nd/62cv0jJg3cKZt+MV8g88WJQqRIhR+Ey/jpsur2fFC26gx+tMryrj2zQt5en9nRuWV5cY7L1zIj198ZdxNPBeqz5rBjzdfl7WEJxNTohApQmvueSq0w7UYTHaymExdukSh1WNFClBXT3/kScJ4YwTQWOVmkZZXp/heiYeGx4rk2FSXk6iaWUn/YOqlHyY7pj9VeXUWx/qfyfBRyR9KFCI5NLZfYXilz4aDx0bdMEeuAAqjh2q++lo/ZrD+kiU83dwZ6c06m2P9z2RugOQH9VGI5FA2+xXOZNinOoElFXVmi+SJ8zY9Om728JlSh69kkzqzRfLAs7/sTLkvVaduTVUiZYeyOnwlV9RHIZKBM5nBO7J8bqKCrp4Bls6dzrHX+kfNWcikUzesQ1kdvpIrShRS1KJ+1GSqfWM7p7t6BigzuPP6C5leWX5GnbrqQ5C4qI9CilY2lp+YXlFGRZlx6vT42cmV5cnmosksYz3Zh8mI5Ere9VGY2Twze8LMDgSvZ4ccc66ZPW1m+8ysyczuiCNWKVzpHliTaXnfwFBokoBkgpjssw7ainQmtRS3uDqzNwE73X0lsDPYHmsA+IS7XwRcDXzMzFblMEYpcFHflNN1NGvGsRSTuBLFzcC24P02YMPYA9z9sLs/H7w/AewD1CgrGZubCO+Cm+xyElWJShKV5aPKhjuTN66rDd1361XnpvyMSKGJK1EsdvfDkEwIwKJ0B5vZCmA18Fz0oUkx2HXwVbqDDuSR0t3EU5V//qaLufuWS6mpSry+/tHw0tUbVteE7vu7DZem/IxIoYmsM9vMngSWhOz6DLDN3atGHPuqu4/rpwj2zQa+D3zB3ben+b7bgdsBli1bduXBgwenEL0UsqMn+7jxKz9kWkUZf3rN+Xz1qV9phrLIBPJuZraZNQPXuPthM1sKPOPu4+rkZlYJfAeod/cvZXp+jXoqXQODQ/z+vT/l+UOvsv3P3s7F1WfFHZJIQUiXKOKaR/EIcBtwT/D68NgDzMyAe4F9k0kSUvimsrrq8DpKt771XCUJkSyJq0YxH3gAWAYcAj7o7sfMrBr4uruvN7N3AD8AGoHhaayfdvcJn7iuGkXhmuzch+GH3Yd9Rn0CIpnLu6anqClRFK7Jrq463Fk9FPKfsSa3iWQuH5ueREJNdu5DWII403OJSDitHit5JdWENK2uKhIfJQrJKxvX1TKjYvR/lhNNYEs16U2T20SyQ01Pklc2rK6hqa2Lf/7BSxhodVWRPKBEIXln7oxKAPZ8/j3MCd4Dr8+EDpNun4hMjZqeJO80tXVz3oJZo5KEiMRHiULyzt62LlZVz407DBEJKFFIXul6rZ+WV3u4WIlCJG8oUUheaTrcBcAlWn5DJG8oUUheaWrtBlCNQiSPKFFIXmlq62LJ3BnMnz097lBEJKBEIXmlqa2bS2pUmxDJJ0oUkjd6Tg/yq86TrFL/hEheUaKQvLHvSDdDrv4JkXyjRCF5o6kt2ZF9SY1qFCL5RIlC8kZTaxdVMyupPmtG3KGIyAhKFJI3mtq6ubh6LpZiSXERiYcSheSF/sEhmo+c0EQ7kTykRCF54UD7SU4PDmmNJ5E8pEQheaGpLbl0x8WqUYjkHSUKyQtNbd3MnFbOeQtmxR2KiIyhRCF5oamti4uWzqW8TB3ZIvlGiUJiNzTk/CIY8SQi+UeJQmJ38NhrnDo9qBFPInkqlkRhZvPM7AkzOxC8np3m2HIz221m38lljJI7e1uTHdka8SSSn+KqUWwCdrr7SmBnsJ3KHcC+nEQlsWhq66ay3Lhw8Zy4QxGREHElipuBbcH7bcCGsIPM7BzgfcDXcxOWxKGprYsLF89hWoVaQkXyUVz/Zy5298MAweuiFMd9GbgLGJrohGZ2u5k1mFlDZ2dn1gKVaLn760t3iEh+qojqxGb2JLAkZNdnMvz8jUCHu+8ys2smOt7dtwJbAerq6jzzSCVOR7p7OXbqtCbaieSxyBKFu1+fap+ZtZvZUnc/bGZLgY6Qw9YAN5nZemAGMNfM7nP3D0cUskzRjt2tbKlvpu14D9VVCTauq2XD6pq05X/znV8A8L+fOsBZiUo2rK6J+SpEZCxzz/2PbzPbArzi7veY2SZgnrvfleb4a4BPuvuNmZy/rq7OGxoashKrZGbH7lY2b2+kp3/w9bJEZTnvv7KGB3e1Zlx+9y2XKlmIxMDMdrl7Xdi+CfsozGyxmd1rZt8NtleZ2UenGNM9wLvN7ADw7mAbM6s2s8emeG6JwZb65lE3fYCe/kHu/8mh0PL7UpRvqW+OPFYRmZxMOrO/CdQD1cH2L4E7p/Kl7v6Ku1/n7iuD12NBeZu7rw85/plMaxMSj7bjPaHlk62vpjqPiMQnk0SxwN0fIBh55O4DwGD6j0ipWTR3emh5eYqHEKUqr65KZC0mEcmOTBLFKTObT/Dj0MyuBroijUoKzvJ5M8eVJSrLufWqc0lUlmdcvnFdbaRxisjkZTLq6S+BR4ALzOxHwELgA5FGJQWl+cgJGg6+yrW1C/hl+6lxo5vqls8LHfWUqlxE8ktGo57MrAKoBQxodvf+qAObCo16yq3/vu1nPPfSMZ7deC1nz5oWdzgicgbSjXqasEZhZn8wpugtZoa7/9+sRCcF7acvHePJfR3cdUOtkoRIkcqk6em3RryfAVwHPA8oUZQ4d+fu7+5j8dzpfOTt58UdjohEZMJE4e7/c+S2mZ0F/L/IIpKCUd/Uzu5Dx7nnlktJTCuf+AMiUpDOZFHA14CV2Q5ECsvA4BBfrN/PBQtn8YErz4k7HBGJUCZ9FP/JG/OmyoBVwANRBiW5cybrM22pb6Y1mBj30TUrqCjX8uAixWzCUU9m9q4RmwPAQXdviTSqKdKop8yErc80vaKMdRcvpr6pnb6BodHlqxZR/4uOUeWJyjLuvuUyDWsVKXDpRj3Fsihg1JQoMrPmnqderxlMRU1Vgh9tWpuFiEQkLmc0PNbMThC+VI8B7u560kyBy9a6SlqfSaS4pUwU7q4HGBe56qpEaI2i3IzBkJpmqnKtzyRS3DLuhTSzRWa2bPgvyqAkNzauq6VszNp8Wp9JRMbKZNTTTcA/kVxmvANYDuwDLo42NInadRclH1U+e3oFp/oGtD6TiITKZGb23wJXA0+6+2ozuxa4NdqwJBd+eOAoQw733lbHVefPH7Vvw+qa0ASQqlxEilcmTU/97v4KUGZmZe7+NHBFtGFJLuzc38HcGRVcufzsuEMRkTyWSY3iuJnNBp4F7jezDpLzKaSADQ05zzR3cE3tIk2YE5G0MrlD3Exy2Y6PA48DvwJ+O8qgJHp7Wrs4evI0a9+8KO5QRCTPZVKjuB34j2A29raI45EceWpfO2UG77pwYdyhiEiey6RGMReoN7MfmNnHzGxx1EFJ9J5q7uDK5WfrGRIiMqEJE4W7/7W7Xwx8jOQQ2e+b2ZORRyaROdLVy97Wbq5Vs5OIZGAyvZgdwBHgFUB3mAL2dHMHANe9WZVDEZnYhInCzP6HmT0D7AQWAH/s7pdFHZhE56n9HdRUJbhw8ey4QxGRApBJZ/Zy4E53fyFbX2pm84BvASuA3wD/zd1fDTmuCvg6cAnJBQr/yN3/K1txlKLe/kF+eOAoH7jyHMxs4g+ISMnLpI9iE9BoZtVZXOtpE7DT3VeSrKlsSnHc/wIed/c3A5eTXDpEpuC5l47R0z/I2ovUeigimclkrac/Bz4PtAPDT6xxYCrNTzcD1wTvtwHPAJ8a871zgXcCfwjg7qeB01P4TiE5LDZRWc7bxizZISKSSiZNT3cCtcEyHtmy2N0PA7j7YTML+3l7PtAJ/IuZXQ7sAu5w91NhJzSz20nO+WDZMi1uG8bd2bm/gzVvms+MMavAioikksmop5eBrsme2MyeNLO9IX83Z3iKCuAtwNfcfTVwitRNVLj7Vnevc/e6hQs1iSzMix0naXm1h7Ua7SQik5BJjeLXwDNm9ijQN1zo7l9K9yF3vz7VPjNrN7OlQW1iKcmht2O1AC3u/lyw/W3SJApJPgM71RLgO3a38tmH9wLwlZ0HmDmtXKvAikhGMkkUh4K/acFfNjwC3AbcE7w+PPYAdz9iZi+bWa27NwPXAb/I0vcXtLCEALB5eyM9/YMAtB7v4VMP7uGVU8nc/sXHm+kbSHYxHenuZfP2RgAlCxGZkHnIoy1DDzSblap/YNJfajYfeABYRjIJfdDdj5lZNfB1d18fHHcFyeGx00jWbD4SNox2rLq6Om9oaMhGqHlnx+7WUQkBoKLMKDM4PZjZv+WwmqoEP9q0NtshikgBMrNd7l4Xti+TUU9vA+4FZgPLgo7lP3H3PzvTgIKO8etCytuA9SO2XwBCAy9VW+qbRyUJgIGhySWIYW0hz8sWERkrk87sLwPrSC7dgbv/nOSwVYnBZG/uNVUJaqoSofuqU5SLiIyU0VpP7v7ymKLB0AMlcqlu7lWJShJjhrwmKsvZuK6WjetqU+4TEZlIJp3ZL5vZ2wE3s2nAX6AZ0rHZuK6WTdv30Ns/9HpZorKcz990MUDKUU8T7RMRSWXCzmwzW0ByKY3rAQO+R3LiWzYn4GVVMXdmA/zDd/fzte//Ckg2LemmLyJTNaXObHc/Cvxe1qOSMzYnkfxn+/ln38NZMytjjkZEil0mo56+ElLcBTS4+7j5DxK9xpYuls+fqSQhIjmRSWf2DOAK4EDwdxkwD/iomX05ssgkpT0tXVxac1bcYYhIicikM/tNwFp3HwAws6+R7Kd4N9AYYWwS4tip07Qe7+G2ty+POxQRKRGZ1ChqgFkjtmcB1e4+yIi1nyQ3GluT6zNeWlMVbyAiUjIyqVF8EXgheByqkZxs9/dmNgt4MsLYJERjy3EALqmZG28gIlIyMhn1dK+ZPQa8lWSi+HSw1AbAxiiDk/H2tHRx/oJZzJmhjmwRyY2UTU9m9ubg9S3AUpLPpTgELAnKJAaNrV1ceo46skUkd9LVKP6S5BPj/ilknwNadjTHOk/0cbirVyOeRCSnUiYKd789eL02d+FIOnuDjuzLzqmKNxARKSkTjnoys5lm9ldmtjXYXmlmN0Yfmoy1p6ULM7i4Wh3ZIpI7mQyP/RfgNPD2YLsF+LvIIpKUGluPc8HC2cyanslgNRGR7MgkUVzg7l8E+gHcvYfk6CfJsT0tXVym/gkRybFMEsVpM0uQ7MDGzC5AE+1yrr27l44TfRrxJCI5l0kbxueAx4Fzzex+YA3wh1EGJeM1tgx3ZCtRiEhuZTLh7gkzex64mmST0x3B0uOSQ3tauygzWLVUiUJEciujXtHgIUWPRhyLpNHYcpyVi+aQmFY+8cEiIlmU0TOzJV7urhnZIhIbJYoCcLirl6MnT6t/QkRikW6tpxlmdqeZ/R8z+xMzy9rgfTObZ2ZPmNmB4PXsFMd93MyazGyvmf2bmc3IVgyF5I2lxZUoRCT30tUotgF1JB9O9F7C13w6U5uAne6+EtgZbI9iZjXAXwB17n4JUA58KIsxFIzGli4qyoyLlmpGtojkXrpawip3vxTAzO4FfprF770ZuCZ4vw14BvhUivgSZtYPzATaQo4pentau1i5eA4zKtWRLSK5l65G0T/8ZvgxqFm02N0PB+c+DCwae4C7twL/SHJp88NAl7t/L9UJzex2M2sws4bOzs4shxsfd6ex5bhmZItIbNLVKC43s+7gvZH8Zd8dvHd3T9sOYmZPAktCdn0mk8CCfoubgfOA48B/mNmH3f2+sOPdfSuwFaCurs4z+Y5c27G7lS31zbQd76G6KsHGdbVsWF2Ttvzu7+7j1df6ebzpCG+7YD4bVtfEfRkiUmLSLTM+pXYOd78+1T4zazezpe5+2MyWAh0hh10PvOTuncFntpNcmDA0UeS7Hbtb2by9kZ7+QQBaj/eweXsjDQeP8eCu1gnLu3r62by9EUDJQkRyKt2op7Uj3p83Zt8tU/zeR4Dbgve3AQ+HHHMIuDpY5tyA64B9U/ze2Gypb379pj+sp3+Qf33uUGj5/SnKt9Q3Rx6riMhI6foo/nHE+wfH7PurKX7vPcC7zewA8O5gGzOrDp7Pjbs/B3wbeJ7kyKsygqalQtR2vCe0fChFI5mnKE91HhGRqKTro7AU78O2JyVYEuS6kPI2YP2I7c+RXJSw4FVXJWgNucmXWXiySFVeXZWIIDoRkdTS1Sg8xfuwbZnAxnW1VJaPzq+JynJ+96plJMYMe01XvnFdbeSxioiMlK5Gcb6ZPUKy9jD8nmD7vNQfkzAbVtfwTHMHO15ow2DU6Ka65fNCRz2lKhcRySXzFI3hZvaudB909+9HElEW1NXVeUNDQ9xhjPP3j+3jmz/+Dc1/ewPJ/nkRkfxgZrvcvS5sX7rhsa8nAjNbGJQVz0y2GLR397Jk7gwlCREpKOmGx5qZfc7MjgL7gV+aWaeZfTZ34RWXI13JRCEiUkjSdWbfCbwD+C13n+/uZwNXAWvM7OO5CK7YtHf3smju9LjDEBGZlHSJ4g+AW939peECd/818OFgn0yCu9Pe3acahYgUnHSJojLs2dhBP0VldCEVp+7eAXr6B1msRCEiBSZdojh9hvskREd3LwCLz1KiEJHCksnqscNDdIbH0Rqgu90kHRlOFHPURyEihSWy1WNltPbuPgCWqEYhIgUmZaIInk/9p8CbgD3ANyJ4gFHJaB+uUaiPQkQKTKbPzF5Pdp+ZXXKOdPVyVqJSjzMVkYIT1zOzS87wrGwRkUIT1zOzS44m24lIoYrsmdkyWnt3HxcunhN3GCIik6ZRTzkwOOR0nuxTR7aIFKR0TU+SJa+c7GNwyDXZTkQKkhJFDmiynYgUMiWKHDjSlUwUmmwnIoVIiSIH2k8Es7LVRyEiBUiJIgfau3opLzPmz1bTk4gUHiWKHGjv7mXh7OmUl+kRqCJSeJQocuBIdy+LNdlORApULInCzD5oZk1mNmRmdWmOu8HMms3sRTPblMsYs6mjW3MoRKRwxVWj2AvcAjyb6gAzKwe+CrwXWAXcamarchNediVrFEoUIlKY0i3hERl33wdglrbN/q3Ai8FzujGzfwduBn4ReYBZ1Ns/SFdPv4bGikjByuc+ihrg5RHbLUFZKDO73cwazKyhs7Mz8uAypedQiEihi6xGYWZPAktCdn3G3R/O5BQhZR5SltzhvhXYClBXV5fyuFwbnmynzmwRKVSRJQp3v36Kp2gBzh2xfQ7QNsVz5pwm24lIocvnpqefASvN7DwzmwZ8CHgk5pgmrT2oUSxSohCRAhXX8NjfMbMW4G3Ao2ZWH5RXm9lj8PrDkv4cqAf2AQ+4e1Mc8U5Fe3cvicpy5s6IZdyAiMiUxTXq6SHgoZDyNpLP5x7efgx4LIehZd3wZLsJRniJiOStfG56KgqabCcihU6JImJHuns1h0JECpoSRYTcXbOyRaTgKVFEqKunn9MDQ0oUIlLQlCgi9PojUDXZTkQKmBJFhNq7NdlORAqfEkWE2ru0zpOIFD4liggNLwi4SE1PIlLAlCgidKS7l7NnVjK9ojzuUEREzpgSRYTaNdlORIqAEkWE2jXZTkSKgBJFhI5097J4jhKFiBQ2JYqIDAwOcfRkH4tVoxCRAqdEEZHOk324a7KdiBQ+JYqIaLKdiBQLJYqIHNFkOxEpEkoUEek4oUQhIsVBiSIiR7p6qSgz5s+aFncoIiJTokQRkSPdvSyaM52yMj0CVUQKmxJFRDq6NTRWRIqDEkVENNlORIqFEkVEtHyHiBSLirgDyHc7dreypb6ZtuM9VFcl2Liulg2ra9KW/8Pj+znRO8D251u44twqNqyuifsyRETOmLl73DFkXV1dnTc0NEz5PDt2t7J5eyM9/YOvlyUqy3n/lTU8uKs14/K7b7lUyUJE8pqZ7XL3urB9sTQ9mdkHzazJzIbMLDQwMzvXzJ42s33BsXfkOs4t9c2jbvoAPf2D3P+TQ5Mq31LfHHmsIiJRiauPYi9wC/BsmmMGgE+4+0XA1cDHzGxVLoIb1na8J7Q8VR0sVXmq84iIFIJYEoW773P3tD+z3f2wuz8fvD8B7ANy2n5TXZUILU81NaI8RXmq84iIFIKCGPVkZiuA1cBzaY653cwazKyhs7MzK9/7yfdcyNh7f6KynN+9ahmJyvJx5bemKN+4rjYr8YiIxCGyUU9m9iSwJGTXZ9z94UmcZzbwIHCnu3enOs7dtwJbIdmZPclwQ509axoOVM2spOu1/lGjm+qWzwsd9ZSqXESkUEWWKNz9+qmew8wqSSaJ+919+9Sjmpx//sGvWTJ3Bs/edS3TKkZXvjasrglNAKnKRUQKVd42PZmZAfcC+9z9S7n+/r2tXfzoxVf4yJoV45KEiEgpiWt47O+YWQvwNuBRM6sPyqvN7LHgsDXA7wNrzeyF4G99rmLc+uyvmT29gluvWparrxQRyUuxzMx294eAh0LK24D1wfsfwri+5JxoefU1Hm08zB+tWcHcGZVxhCAikjfUphLi3h++hAEfWXNe3KGIiMROiWKMrtf6+dbPXuamy6s1/0FEBCWKce577iCvnR7kj995ftyhiIjkBa0eG9ixu5UvPr6ftq5epleU0XzkBBctnRt3WCIisVOiYPwqsX0DQ2ze3gigOREiUvLU9ETqVWK16quIiBIFkHp1V636KiKiRAGkXt1Vo55ERJQoANi4rlarvoqIpKDObN7osNaqryIi4ylRBLTqq4hIODU9iYhIWkoUIiKSlhKFiIikpUQhIiJpKVGIiEha5u5xx5B1ZtYJHJzgsAXA0RyEk2903aVF111apnLdy919YdiOokwUmTCzBnevizuOXNN1lxZdd2mJ6rrV9CQiImkpUYiISFqlnCi2xh1ATHTdpUXXXVoiue6S7aMQEZHMlHKNQkREMqBEISIiaZVcojCzG8ys2cxeNLNNcccTFTP7hpl1mNneEWXzzOwJMzsQvJ4dZ4xRMLNzzexpM9tnZk1mdkdQXtTXbmYzzOynZvbz4Lr/Oigv6useZmblZrbbzL4TbJfKdf/GzBrN7AUzawjKsn7tJZUozKwc+CrwXmAVcKuZrYo3qsh8E7hhTNkmYKe7rwR2BtvFZgD4hLtfBFwNfCz4Ny72a+8D1rr75cAVwA1mdjXFf93D7gD2jdgulesGuNbdrxgxfyLr115SiQJ4K/Ciu//a3U8D/w7cHHNMkXD3Z4FjY4pvBrYF77cBG3IZUy64+2F3fz54f4LkzaOGIr92TzoZbFYGf06RXzeAmZ0DvA/4+ojior/uNLJ+7aWWKGqAl0dstwRlpWKxux+G5A0VWBRzPJEysxXAauA5SuDag+aXF4AO4Al3L4nrBr4M3AUMjSgrheuG5I+B75nZLjO7PSjL+rWX2hPuLKRM44OLkJnNBh4E7nT3brOwf/ri4u6DwBVmVgU8ZGaXxBxS5MzsRqDD3XeZ2TUxhxOHNe7eZmaLgCfMbH8UX1JqNYoW4NwR2+cAbTHFEod2M1sKELx2xBxPJMyskmSSuN/dtwfFJXHtAO5+HHiGZB9VsV/3GuAmM/sNyabktWZ2H8V/3QC4e1vw2gE8RLJ5PevXXmqJ4mfASjM7z8ymAR8CHok5plx6BLgteH8b8HCMsUTCklWHe4F97v6lEbuK+trNbGFQk8DMEsD1wH6K/LrdfbO7n+PuK0j+//yUu3+YIr9uADObZWZzht8D7wH2EsG1l9zMbDNbT7JNsxz4hrt/Id6IomFm/wZcQ3LZ4Xbgc8AO4AFgGXAI+KC7j+3wLmhm9g7gB0Ajb7RZf5pkP0XRXruZXUay47Kc5A/AB9z9b8xsPkV83SMFTU+fdPcbS+G6zex8krUISHYj/Ku7fyGKay+5RCEiIpNTak1PIiIySUoUIiKSlhKFiIikpUQhIiJpKVGIiEhaShQiOWRmP447BpHJ0vBYERFJSzUKkRwys5MTHyWSX5QoREQkLSUKERFJS4lCRETSUqIQEZG0lChERCQtDY8VEZG0VKMQEZG0lChERCQtJQoREUlLiUJERNJSohARkbSUKEREJC0lChERSev/A+qcNjsUWRRkAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Create x-axis\n",
    "iv = np.linspace(1,ev.size,ev.size,endpoint=True)\n",
    "\n",
    "# Plot\n",
    "plt.plot(iv,ev,'o-')\n",
    "plt.xlabel('i')\n",
    "plt.ylabel('PDEP eigenvalue')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see the eigenvalues of the dielectric response decay to zero (see [J. Chem. Theory Comput. 11, 2680 (2015)](https://doi.org/10.1021/ct500958p), and references therein). The number of PDEPs is a parameter of the simulation that is system dependent and needs to be converged. Typically the number of PDEPs is set equal to multiple of the number of electrons."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 3: Calculation of quasiparticle corrections"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The GW electronic structure is computed treating the frequency integration of the correlation part of the self energy with the contour deformation techinique and by computing the dielectric screening at multipole frequencies with Lanczos iterations. Check out the ``wfreq.x`` [input description](http://www.west-code.org/doc/West/latest/) and generate an input file for WEST called ``wfreq.in``."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Download this file in your current working directory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "wget -N -q http://www.west-code.org/doc/training/silane/wfreq.in"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's inspect the ``wfreq.in`` file, input for ``wfreq.x``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "input_west:\n",
      "    qe_prefix: silane\n",
      "    west_prefix: silane\n",
      "    outdir: ./\n",
      "\n",
      "wstat_control:\n",
      "    wstat_calculation: S\n",
      "    n_pdep_eigen: 50\n",
      "\n",
      "wfreq_control:\n",
      "    wfreq_calculation: XWGQ\n",
      "    n_pdep_eigen_to_use: 50\n",
      "    qp_bandrange: [1,5]\n",
      "    n_refreq: 300\n",
      "    ecut_refreq: 2.0"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cat wfreq.in"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We compute the quasiparticle corrections of states identified by band indexes $1,2,3,4,5$. We have set PDEPs to 50, this number can be reduced in order to check convergence. For the Contour Deformation technique, the frequency dependence of the dielectric screening is sampled in the energy window $[0,2]$Ry using 300 points.\n",
    "\n",
    "According to perturbation theory, the quasiparticle energy $E_{kn}$ of the state with band index $n$ and k-point $k$ is computed as:\n",
    "\n",
    "$E_{kn} = \\varepsilon_{kn} + \\Sigma^X_{kn} + \\Sigma^C_{kn}(E_{kn}) - V^{xc}_{kn}$\n",
    "\n",
    "where $\\varepsilon_{kn}$ is the mean-field (or Kohn-Sham) single-particle energy (obtained in Step 1), $\\Sigma^X_{kn}$, $\\Sigma^C_{kn}(E_{kn})$, and $V^{xc}_{kn}$ are the contributions to the signle-particle energy given by the exchange self-energy, correlation self-energy and the exchange-correlation potential used in Step 1. Note that the correlation self-energy depends on the energy and hence this equation is solved using an iterative solver (e.g., the secant method), or by approximating the self-energy to first order in the frequency."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run ``wfreq.x`` on 2 cores. As for ``wstat.x``, the number of cores may be increased depending on the size of the system and the available computational resources. [Tutorial 4](http://greatfire.uchicago.edu/west-public/West/raw/master/Doc/tutorials/basic/basic_004.ipynb) explains additional flags that can be used to control the distribution of data-structures and loops. In this tutorial, we are not going to use any specific parallelization flag, which means that Fourier transform operations are distributed using the chosen number of cores (2)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "mpirun -n 2 wfreq.x -i wfreq.in > wfreq.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output file ``wfreq.out`` contains information about the calculation of the GW self-energy, and the corrected electronic structure can be found in the file ``<west_prefix>.wfreq.save/wfreq.json``."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the reader does NOT have the computational resources to run the calculation, the WEST output file needed for the next step can be directly downloaded as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "mkdir -p silane.wfreq.save\n",
    "wget -N -q http://www.west-code.org/doc/training/silane/wfreq.json -O silane.wfreq.save/wfreq.json"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we show how to load and print the quasiparticle corrections."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read the output of Wfreq: wfreq.json\n",
    "\n",
    "def wfreq2df(filename='wfreq.json', dfKeys=['eks','eqpLin','eqpSec','sigmax','sigmac_eks','sigmac_eqpLin','sigmac_eqpSec', 'vxcl', 'vxcnl', 'hf']) :\n",
    "\n",
    "    # read data from JSON file\n",
    "\n",
    "    import json\n",
    "    with open(filename) as file :\n",
    "        data = json.load(file)\n",
    "\n",
    "    import numpy as np\n",
    "    import pandas as pd\n",
    "\n",
    "    # build the dataframe\n",
    "    columns = ['k','s','n'] + dfKeys\n",
    "    df = pd.DataFrame(columns=columns)\n",
    "\n",
    "    # insert data into the dataframe\n",
    "    j = 0\n",
    "    for s in range(1,data['system']['electron']['nspin']+1) :\n",
    "        for k in data['system']['bzsamp']['k'] :\n",
    "            kindex = f\"K{k['id']+(s-1)*len(data['system']['bzsamp']['k']):06d}\"\n",
    "            for i, n in enumerate(data['output']['Q']['bandmap']) :\n",
    "                d = data['output']['Q'][kindex]\n",
    "                row = [k['id'], s, n]\n",
    "                for key in dfKeys :\n",
    "                    if 're' in d[key] :\n",
    "                        row.append(d[key]['re'][i])\n",
    "                    else :\n",
    "                        row.append(d[key][i])\n",
    "                df.loc[j] = row\n",
    "                j += 1\n",
    "\n",
    "    # cast the columns k, s, n to int\n",
    "    df['k'] = df['k'].apply(np.int64)\n",
    "    df['s'] = df['s'].apply(np.int64)\n",
    "    df['n'] = df['n'].apply(np.int64)\n",
    "\n",
    "    return df, data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `wfreq2df` function is also defined in the [WESTpy Python package](http://www.west-code.org/doc/westpy/latest/). If WESTpy is installed, simply do"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from westpy import wfreq2df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>k</th>\n",
       "      <th>s</th>\n",
       "      <th>n</th>\n",
       "      <th>eks</th>\n",
       "      <th>eqpLin</th>\n",
       "      <th>eqpSec</th>\n",
       "      <th>sigmax</th>\n",
       "      <th>sigmac_eks</th>\n",
       "      <th>sigmac_eqpLin</th>\n",
       "      <th>sigmac_eqpSec</th>\n",
       "      <th>vxcl</th>\n",
       "      <th>vxcnl</th>\n",
       "      <th>hf</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>-13.236949</td>\n",
       "      <td>-16.274080</td>\n",
       "      <td>-16.135752</td>\n",
       "      <td>-17.606357</td>\n",
       "      <td>1.764879</td>\n",
       "      <td>3.576503</td>\n",
       "      <td>3.438446</td>\n",
       "      <td>-11.249696</td>\n",
       "      <td>0.0</td>\n",
       "      <td>-6.356661</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>2</td>\n",
       "      <td>-8.231838</td>\n",
       "      <td>-12.152440</td>\n",
       "      <td>-12.047982</td>\n",
       "      <td>-15.766143</td>\n",
       "      <td>0.009841</td>\n",
       "      <td>0.729477</td>\n",
       "      <td>0.706368</td>\n",
       "      <td>-11.243026</td>\n",
       "      <td>0.0</td>\n",
       "      <td>-4.523118</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>3</td>\n",
       "      <td>-8.230714</td>\n",
       "      <td>-12.148676</td>\n",
       "      <td>-12.044134</td>\n",
       "      <td>-15.765430</td>\n",
       "      <td>0.012755</td>\n",
       "      <td>0.731935</td>\n",
       "      <td>0.708819</td>\n",
       "      <td>-11.242587</td>\n",
       "      <td>0.0</td>\n",
       "      <td>-4.522843</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>4</td>\n",
       "      <td>-8.230556</td>\n",
       "      <td>-12.147748</td>\n",
       "      <td>-12.043169</td>\n",
       "      <td>-15.765269</td>\n",
       "      <td>0.013611</td>\n",
       "      <td>0.732684</td>\n",
       "      <td>0.709564</td>\n",
       "      <td>-11.242489</td>\n",
       "      <td>0.0</td>\n",
       "      <td>-4.522780</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>5</td>\n",
       "      <td>-0.465586</td>\n",
       "      <td>0.665732</td>\n",
       "      <td>0.664620</td>\n",
       "      <td>-0.587282</td>\n",
       "      <td>-0.348920</td>\n",
       "      <td>-0.372719</td>\n",
       "      <td>-0.372694</td>\n",
       "      <td>-2.090184</td>\n",
       "      <td>0.0</td>\n",
       "      <td>1.502902</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   k  s  n        eks     eqpLin     eqpSec     sigmax  sigmac_eks  \\\n",
       "0  1  1  1 -13.236949 -16.274080 -16.135752 -17.606357    1.764879   \n",
       "1  1  1  2  -8.231838 -12.152440 -12.047982 -15.766143    0.009841   \n",
       "2  1  1  3  -8.230714 -12.148676 -12.044134 -15.765430    0.012755   \n",
       "3  1  1  4  -8.230556 -12.147748 -12.043169 -15.765269    0.013611   \n",
       "4  1  1  5  -0.465586   0.665732   0.664620  -0.587282   -0.348920   \n",
       "\n",
       "   sigmac_eqpLin  sigmac_eqpSec       vxcl  vxcnl        hf  \n",
       "0       3.576503       3.438446 -11.249696    0.0 -6.356661  \n",
       "1       0.729477       0.706368 -11.243026    0.0 -4.523118  \n",
       "2       0.731935       0.708819 -11.242587    0.0 -4.522843  \n",
       "3       0.732684       0.709564 -11.242489    0.0 -4.522780  \n",
       "4      -0.372719      -0.372694  -2.090184    0.0  1.502902  "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "df, data = wfreq2df('silane.wfreq.save/wfreq.json')\n",
    "display(df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All energies in the plot are reported in eV. The full-frequency $G_0W_0$ energies correspond to the ``eqpSec`` column.\n",
    "\n",
    "From the table we see that HOMO has an energy of -8.231 eV at PBE, while HOMO has an energy of -12.148 eV at $G_0W_0$\\@PBE."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Explanation of keys:\n",
    "\n",
    "- `k`: k-point index\n",
    "- `s`: spin index\n",
    "- `n`: state index\n",
    "- `eks` : $\\varepsilon_{kn}$, Kohn-Sham energy (obtained in Step 1)\n",
    "- `eqpLin` : quasiparticle energy (full-frequency $G_0W_0 @ PBE$), obtained by approximating the self-energy to first order in the frequency\n",
    "- `eqpSec` : $E_{kn}$,quasiparticle energy (full-frequency $G_0W_0 @ PBE$), obtained by using the secant method to solve the frequency-dependency in the quasiparticle equation\n",
    "- `sigmax` : exchange self-energy\n",
    "- `sigmac_eks` : correlation self-energy, evaluated at `eks`. Note that `re` and `im` identify the real and imaginary parts.\n",
    "- `sigmac_eqpLin` : correlation self-energy, evaluated at `eqpLin`\n",
    "- `sigmac_eqpSec` : correlation self-energy, evaluated at `eqpSec`\n",
    "- `vxcl` : contribution to the energy given by the semi-local xc functional\n",
    "- `vxcnl` : contribution to the energy given by the excact exchange (if the xc is hybrid)\n",
    "- `hf` : quasiparticle energy according to perturbative Hartree-Fock, i.e., no correlation self-energy\n",
    "- `z` : the value of the z-parameter used to approximate the self-energy to first order in the frequency (definition given in [J. Chem. Theory Comput. 11, 2680 (2015)](https://doi.org/10.1021/ct500958p)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the following we show how it is possible to extract auxilirary information about the system:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "=== AUX information ===\n",
      "volume [a.u.^3]  : 8000.0\n",
      "a1 [a.u.]        : [20.0, 0.0, 0.0]\n",
      "a2 [a.u.]        : [0.0, 20.0, 0.0]\n",
      "a3 [a.u.]        : [0.0, 0.0, 20.0]\n",
      "b1 [a.u.]        : [0.3141592653589793, 0.0, 0.0]\n",
      "b2 [a.u.]        : [0.0, 0.3141592653589793, 0.0]\n",
      "b3 [a.u.]        : [0.0, 0.0, 0.3141592653589793]\n",
      "# electrons      : 8.0\n",
      "# bands          : 10\n",
      "# spin           : 1\n",
      "ecutwfc [Ry]     : 25.0\n",
      "FFT grid         : [64, 64, 64]\n",
      "k: 1, crystcoord : [0.0, 0.0, 0.0]\n"
     ]
    }
   ],
   "source": [
    "print('=== AUX information ===')\n",
    "print(f\"volume [a.u.^3]  : {data['system']['cell']['omega']}\")\n",
    "print(f\"a1 [a.u.]        : {data['system']['cell']['a1']}\")\n",
    "print(f\"a2 [a.u.]        : {data['system']['cell']['a2']}\")\n",
    "print(f\"a3 [a.u.]        : {data['system']['cell']['a3']}\")\n",
    "print(f\"b1 [a.u.]        : {data['system']['cell']['b1']}\")\n",
    "print(f\"b2 [a.u.]        : {data['system']['cell']['b2']}\")\n",
    "print(f\"b3 [a.u.]        : {data['system']['cell']['b3']}\")\n",
    "print(f\"# electrons      : {data['system']['electron']['nelec']}\")\n",
    "print(f\"# bands          : {data['system']['electron']['nbnd']}\")\n",
    "print(f\"# spin           : {data['system']['electron']['nspin']}\")\n",
    "print(f\"ecutwfc [Ry]     : {data['system']['basis']['ecutwfc:ry']}\")\n",
    "print(f\"FFT grid         : {data['system']['3dfft']['p']}\")\n",
    "for k in data['system']['bzsamp']['k'] :\n",
    "    print(f\"k: {k['id']}, crystcoord : {k['crystcoord']}\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
