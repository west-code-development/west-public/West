{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "349f3a17",
   "metadata": {},
   "source": [
    "This tutorial can be downloaded [link](http://greatfire.uchicago.edu/west-public/West/raw/master/Doc/tutorials/basic/basic_008.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "209f4038",
   "metadata": {},
   "source": [
    "# Intro tutorial 8: Embedded Bethe-Salpeter Equation (eBSE) calculations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3f96e88a",
   "metadata": {},
   "source": [
    "In this tutorial, we show how to perform embedded BSE (eBSE) calculations with the WEST code. Within eBSE, we formulate an effective Hamiltonian for a selected number of orbitals in the form\n",
    "$$\n",
    "H^{\\mathrm{eBSE}}_{vc, v'c'} = \\left( \\epsilon^{GW}_{c} - \\epsilon^{GW}_{v} \\right)\\delta_{vv'}\\delta_{cc'} - W_{cv, c'v'} + V_{cv, c'v'},\n",
    "$$\n",
    "where $v$ and $v'$ include all occupied states in the active space, $c$ and $c'$ all unoccupied states. $\\epsilon^{GW}$ are the quasiparticle energies in the $\\mathrm{G_0W_0}$ approximation.\n",
    "\n",
    "Matrix elements of the direct interaction $W_{cv, c'v'}$ are defined as\n",
    "$$\n",
    "w_{cv,c'v'} = \\int d^3\\mathbf{r} d^3\\mathbf{r}' \\phi_{v}(\\mathbf{r})\\phi_{v'}(\\mathbf{r})W_0(\\mathbf{r},\\mathbf{r}',\\omega=0) \\phi_{c}(\\mathbf{r}')\\phi_{c'}(\\mathbf{r}'),\n",
    "$$\n",
    "where $W_0(\\mathbf{r},\\mathbf{r}',\\omega)$ is the screened Coulomb potential in the random-phase approximation (RPA). These matrix elements describe the Coulomb attraction between the excited electrons and the valence holes.\n",
    "\n",
    "Matrix elements of the repulsive exchange interaction $V_{cv, c'v'}$ are given by\n",
    "$$\n",
    "V_{cv,c'v'} = \\int d^3\\mathbf{r} d^3\\mathbf{r}'  \\phi_{v}(\\mathbf{r})\\phi_{c}(\\mathbf{r})W^R_0(\\mathbf{r},\\mathbf{r}',\\omega=0) \\phi_{v'}(\\mathbf{r}')\\phi_{c'}(\\mathbf{r}'),\n",
    "$$\n",
    "where $W^R_0(\\mathbf{r},\\mathbf{r}',\\omega)$ is the Coulomb potential in the constrained random-phase approximation (cRPA).\n",
    "\n",
    "The eigenvalues and -states of the eBSE Hamiltonian yield the neutral excitations of the system, i.e.\n",
    "$$\n",
    "H^{\\mathrm{eBSE}}X^\\lambda = E^\\lambda X^\\lambda,\n",
    "$$\n",
    "where $E^\\lambda$ are the neutral excitation energies, and $X^\\lambda$ are the corresponding excited many-body states."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6069a028",
   "metadata": {},
   "source": [
    "eBSE calculations are performed in two separate steps:\n",
    "1. In an initial WEST calculation, the quasiparticle energies and matrix elements $W$ and $V$ are calculated and stored in a JSON file.\n",
    "2. The eBSE Hamiltonian is constructed from the parameters in the JSON file and diagonalized. This step is performed by the [WESTpy Python package](http://www.west-code.org/doc/westpy/latest/)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df09f3f5",
   "metadata": {},
   "source": [
    "## Step 1: Parameters of the eBSE Hamiltonian"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0eca9283",
   "metadata": {},
   "source": [
    "### Step 1.1: Mean-field starting point"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3f4d34bd",
   "metadata": {},
   "source": [
    "As a starting point for the many-body perturbation theory (MBPT) calculation, we perform a density functional theory (DFT) calculation using [Quantum ESPRESSO](https://www.quantum-espresso.org/).\n",
    "\n",
    "Note that embedded BSE calculations require spin-polarized DFT calculations as a starting point. This is distinct from Quantum Defect Embedding Theory (QDET), which generally starts from spin-unpolarized DFT calculations."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce84db0f",
   "metadata": {},
   "source": [
    "Download the following files in your working directory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "6a5f0752",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "wget -N -q http://www.west-code.org/doc/training/nv_diamond_63_spinpol/pw.in\n",
    "wget -N -q http://www.quantum-simulation.org/potentials/sg15_oncv/upf/C_ONCV_PBE-1.0.upf\n",
    "wget -N -q http://www.quantum-simulation.org/potentials/sg15_oncv/upf/N_ONCV_PBE-1.0.upf"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "53eb484f",
   "metadata": {},
   "source": [
    "Let us inspect the `pw.in` file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "441a9f79",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "&CONTROL\n",
      "calculation = 'scf'\n",
      "wf_collect = .true.\n",
      "pseudo_dir = './'\n",
      "/\n",
      "&SYSTEM\n",
      "input_dft = 'PBE'\n",
      "ibrav = 0\n",
      "ecutwfc = 50\n",
      "nosym = .true.\n",
      "tot_charge = -1\n",
      "nspin = 2\n",
      "tot_magnetization = 2\n",
      "nbnd = 176\n",
      "nat = 63\n",
      "ntyp = 2\n",
      "/\n",
      "&ELECTRONS\n",
      "conv_thr = 1D-08\n",
      "/\n",
      "K_POINTS gamma\n",
      "CELL_PARAMETERS angstrom\n",
      "7.136012  0.000000  0.000000\n",
      "0.000000  7.136012  0.000000\n",
      "0.000000  0.000000  7.136012\n",
      "ATOMIC_SPECIES\n",
      "C  12.01099968  C_ONCV_PBE-1.0.upf\n",
      "N  14.00699997  N_ONCV_PBE-1.0.upf\n",
      "ATOMIC_POSITIONS crystal\n",
      "C    0.99996000  0.99996000  0.99996000\n",
      "C    0.12495000  0.12495000  0.12495000\n",
      "C    0.99905000  0.25039000  0.25039000\n",
      "C    0.12350000  0.37499000  0.37499000\n",
      "C    0.25039000  0.99905000  0.25039000\n",
      "C    0.37499000  0.12350000  0.37499000\n",
      "C    0.25039000  0.25039000  0.99905000\n",
      "C    0.37499000  0.37499000  0.12350000\n",
      "C    0.00146000  0.00146000  0.50100000\n",
      "C    0.12510000  0.12510000  0.62503000\n",
      "C    0.00102000  0.24944000  0.74960000\n",
      "C    0.12614000  0.37542000  0.87402000\n",
      "C    0.24944000  0.00102000  0.74960000\n",
      "C    0.37542000  0.12614000  0.87402000\n",
      "C    0.24839000  0.24839000  0.49966000\n",
      "C    0.37509000  0.37509000  0.61906000\n",
      "C    0.00146000  0.50100000  0.00146000\n",
      "C    0.12510000  0.62503000  0.12510000\n",
      "C    0.00102000  0.74960000  0.24944000\n",
      "C    0.12614000  0.87402000  0.37542000\n",
      "C    0.24839000  0.49966000  0.24839000\n",
      "C    0.37509000  0.61906000  0.37509000\n",
      "C    0.24944000  0.74960000  0.00102000\n",
      "C    0.37542000  0.87402000  0.12614000\n",
      "C    0.99883000  0.50076000  0.50076000\n",
      "C    0.12502000  0.62512000  0.62512000\n",
      "C    0.99961000  0.74983000  0.74983000\n",
      "C    0.12491000  0.87493000  0.87493000\n",
      "C    0.25216000  0.50142000  0.74767000\n",
      "C    0.37740000  0.62659000  0.87314000\n",
      "C    0.25216000  0.74767000  0.50142000\n",
      "C    0.37740000  0.87314000  0.62659000\n",
      "C    0.50100000  0.00146000  0.00146000\n",
      "C    0.62503000  0.12510000  0.12510000\n",
      "C    0.49966000  0.24839000  0.24839000\n",
      "C    0.61906000  0.37509000  0.37509000\n",
      "C    0.74960000  0.00102000  0.24944000\n",
      "C    0.87402000  0.12614000  0.37542000\n",
      "C    0.74960000  0.24944000  0.00102000\n",
      "C    0.87402000  0.37542000  0.12614000\n",
      "C    0.50076000  0.99883000  0.50076000\n",
      "C    0.62512000  0.12502000  0.62512000\n",
      "C    0.50142000  0.25216000  0.74767000\n",
      "C    0.62659000  0.37740000  0.87314000\n",
      "C    0.74983000  0.99961000  0.74983000\n",
      "C    0.87493000  0.12491000  0.87493000\n",
      "C    0.74767000  0.25216000  0.50142000\n",
      "C    0.87314000  0.37740000  0.62659000\n",
      "C    0.50076000  0.50076000  0.99883000\n",
      "C    0.62512000  0.62512000  0.12502000\n",
      "C    0.50142000  0.74767000  0.25216000\n",
      "C    0.62659000  0.87314000  0.37740000\n",
      "C    0.74767000  0.50142000  0.25216000\n",
      "C    0.87314000  0.62659000  0.37740000\n",
      "C    0.74983000  0.74983000  0.99961000\n",
      "C    0.87493000  0.87493000  0.12491000\n",
      "N    0.48731000  0.48731000  0.48731000\n",
      "C    0.49191000  0.76093000  0.76093000\n",
      "C    0.62368000  0.87476000  0.87476000\n",
      "C    0.76093000  0.49191000  0.76093000\n",
      "C    0.87476000  0.62368000  0.87476000\n",
      "C    0.76093000  0.76093000  0.49191000\n",
      "C    0.87476000  0.87476000  0.62368000\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cat pw.in"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "07e4244e",
   "metadata": {},
   "source": [
    "We can now run `pw.x` on 2 cores."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f69407d6",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "mpirun -n 2 pw.x -i pw.in > pw.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91637dd2",
   "metadata": {},
   "source": [
    "### Step 1.2: Calculation of the dielectric screening"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dba6ad37",
   "metadata": {},
   "source": [
    "As for GW and QDET calculations within WEST, we first have to determine the dielectric screening before we can proceed to calculate the excitations of the spin defect. In WEST, the dielectric screening is obtained from the projective dielectric eigendecomposition (PDEP) technique. The calculation with `wstat.x` requires an input file `wstat.in`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "6a04ffbc",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "wget -N -q http://www.west-code.org/doc/training/nv_diamond_63_spinpol/wstat.in"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e1ed4e07",
   "metadata": {},
   "source": [
    "Once again, we can have a look at the input file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "c8dafd52",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "wstat_control:\n",
      "  wstat_calculation: S\n",
      "  n_pdep_eigen: 512\n",
      "  trev_pdep: 0.00001\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cat wstat.in"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aefa72a8",
   "metadata": {},
   "source": [
    "As we can see, there are no input parameters in `wstat.in` specific to QDET. We can now execute `wstat.x` with the following command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f3c3c610",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "mpirun -n 2 wstat.x -i wstat.in > wstat.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e18513c7",
   "metadata": {},
   "source": [
    "### Step 1.3: eBSE calculation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9d44e342",
   "metadata": {},
   "source": [
    "The calculation of matrix elements for eBSE within WEST is identical to that of matrix elements for QDET, with the exception of a single keyword: By adding the keyword `l_qdet_verbose: true`, we enable the write-out of both matrix elements of the RPA-screened and cRPA-screened matrix elements (typically only matrix elements of the cRPA-screened potential are written to JSON file)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec71a9fa",
   "metadata": {},
   "source": [
    "You can download the input file with the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "6a6f1c10",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "wget -N -q http://www.west-code.org/doc/training/nv_diamond_63_spinpol/wfreq.in"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29c13f9b",
   "metadata": {},
   "source": [
    "We see that the `wfreq.in` file looks exactly like the input file used for the QDET calculation in [Tutorial 5](http://greatfire.uchicago.edu/west-public/West/raw/master/Doc/tutorials/basic/basic_005.ipynb)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "6ff8f6e1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "wstat_control:\n",
      "  wstat_calculation: S\n",
      "  n_pdep_eigen: 512\n",
      "  trev_pdep: 0.00001\n",
      "\n",
      "wfreq_control:\n",
      "  wfreq_calculation: XWGQH\n",
      "  macropol_calculation: C\n",
      "  l_qdet_verbose: true\n",
      "  l_enable_off_diagonal: true\n",
      "  n_pdep_eigen_to_use: 512\n",
      "  qp_bands: [87, 122, 123, 126, 127, 128]\n",
      "  n_refreq: 300\n",
      "  ecut_refreq: 2.0\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cat wfreq.in"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36c8d3c7",
   "metadata": {},
   "source": [
    "The `wfreq.x` calculation is performed with the following command"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8488f2cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "mpirun -n 2 wfreq.x -i wfreq.in > wfreq.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68d66575",
   "metadata": {},
   "source": [
    "If the reader does NOT have the computational resources to run the calculation, the WEST output file needed for the next step can be directly downloaded as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "a23b5c13",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "mkdir west.wfreq.save\n",
    "wget -N -q http://www.west-code.org/doc/training/nv_diamond_63_spinpol/wfreq.json -O west.wfreq.save/wfreq.json"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17e35386",
   "metadata": {},
   "source": [
    "## Step 2: Diagonalization of the eBSE Hamiltonian"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "419fbb0a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " \n",
      " _    _ _____ _____ _____            \n",
      "| |  | |  ___/  ___|_   _|           \n",
      "| |  | | |__ \\ `--.  | |_ __  _   _  \n",
      "| |/\\| |  __| `--. \\ | | '_ \\| | | | \n",
      "\\  /\\  / |___/\\__/ / | | |_) | |_| | \n",
      " \\/  \\/\\____/\\____/  \\_/ .__/ \\__, | \n",
      "                       | |     __/ | \n",
      "                       |_|    |___/  \n",
      " \n",
      "WEST version     :  5.2.0\n",
      "Today            :  2022-12-13 14:10:12.921036\n",
      "===============================================================\n",
      "Solving eBSE Hamiltonian...\n",
      "===============================================================\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead tr th {\n",
       "        text-align: left;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr>\n",
       "      <th></th>\n",
       "      <th colspan=\"2\" halign=\"left\"></th>\n",
       "      <th colspan=\"6\" halign=\"left\">diag[1RDM - 1RDM(GS)]</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th></th>\n",
       "      <th>E [eV]</th>\n",
       "      <th>char</th>\n",
       "      <th>87</th>\n",
       "      <th>122</th>\n",
       "      <th>123</th>\n",
       "      <th>126</th>\n",
       "      <th>127</th>\n",
       "      <th>128</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0.000</td>\n",
       "      <td>2.993</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0.427</td>\n",
       "      <td>1.081</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.003</td>\n",
       "      <td>-0.003</td>\n",
       "      <td>-0.014</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.021</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0.427</td>\n",
       "      <td>1.081</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.003</td>\n",
       "      <td>-0.003</td>\n",
       "      <td>-0.014</td>\n",
       "      <td>0.021</td>\n",
       "      <td>-0.000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>1.054</td>\n",
       "      <td>1.022</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>0.000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>2.247</td>\n",
       "      <td>2.206</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.002</td>\n",
       "      <td>-0.006</td>\n",
       "      <td>-0.474</td>\n",
       "      <td>0.483</td>\n",
       "      <td>0.000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>5</th>\n",
       "      <td>2.247</td>\n",
       "      <td>2.206</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.002</td>\n",
       "      <td>-0.006</td>\n",
       "      <td>-0.474</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.483</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>6</th>\n",
       "      <td>4.038</td>\n",
       "      <td>2.232</td>\n",
       "      <td>-0.001</td>\n",
       "      <td>-0.005</td>\n",
       "      <td>-0.483</td>\n",
       "      <td>-0.009</td>\n",
       "      <td>0.497</td>\n",
       "      <td>0.000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>7</th>\n",
       "      <td>4.038</td>\n",
       "      <td>2.232</td>\n",
       "      <td>-0.001</td>\n",
       "      <td>-0.005</td>\n",
       "      <td>-0.483</td>\n",
       "      <td>-0.009</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.497</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>8</th>\n",
       "      <td>5.865</td>\n",
       "      <td>2.233</td>\n",
       "      <td>-0.002</td>\n",
       "      <td>-0.487</td>\n",
       "      <td>-0.007</td>\n",
       "      <td>-0.002</td>\n",
       "      <td>0.498</td>\n",
       "      <td>0.000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9</th>\n",
       "      <td>5.865</td>\n",
       "      <td>2.233</td>\n",
       "      <td>-0.002</td>\n",
       "      <td>-0.487</td>\n",
       "      <td>-0.007</td>\n",
       "      <td>-0.002</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.498</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10</th>\n",
       "      <td>9.300</td>\n",
       "      <td>2.236</td>\n",
       "      <td>-0.497</td>\n",
       "      <td>-0.002</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>0.500</td>\n",
       "      <td>0.000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>11</th>\n",
       "      <td>9.300</td>\n",
       "      <td>2.236</td>\n",
       "      <td>-0.497</td>\n",
       "      <td>-0.002</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.500</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                diag[1RDM - 1RDM(GS)]                                   \n",
       "   E [eV]  char                    87    122    123    126    127    128\n",
       "0   0.000 2.993                 0.000  0.000  0.000  0.000  0.000  0.000\n",
       "1   0.427 1.081                -0.000 -0.003 -0.003 -0.014  0.000  0.021\n",
       "2   0.427 1.081                -0.000 -0.003 -0.003 -0.014  0.021 -0.000\n",
       "3   1.054 1.022                -0.000 -0.000 -0.000 -0.000 -0.000  0.000\n",
       "4   2.247 2.206                -0.000 -0.002 -0.006 -0.474  0.483  0.000\n",
       "5   2.247 2.206                -0.000 -0.002 -0.006 -0.474  0.000  0.483\n",
       "6   4.038 2.232                -0.001 -0.005 -0.483 -0.009  0.497  0.000\n",
       "7   4.038 2.232                -0.001 -0.005 -0.483 -0.009  0.000  0.497\n",
       "8   5.865 2.233                -0.002 -0.487 -0.007 -0.002  0.498  0.000\n",
       "9   5.865 2.233                -0.002 -0.487 -0.007 -0.002  0.000  0.498\n",
       "10  9.300 2.236                -0.497 -0.002 -0.000 -0.000  0.500  0.000\n",
       "11  9.300 2.236                -0.497 -0.002 -0.000 -0.000  0.000  0.500"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-----------------------------------------------------\n"
     ]
    }
   ],
   "source": [
    "from westpy.qdet import eBSEResult\n",
    "\n",
    "# construct object for effective Hamiltonian\n",
    "ebse = eBSEResult(filename='west.wfreq.save/wfreq.json', spin_flip_=True)\n",
    "\n",
    "# diagonalize Hamiltonian\n",
    "solution = ebse.solve()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e51da318",
   "metadata": {},
   "source": [
    "Calling the function `solve()` of the object `eBSEResult` writes the excitation energies (in eV), spin multiplicity and relative occupation (compared to the occupation of the ground state) to screen in a manner similar to the output of a QDET calculation. See also [Tutorial 5](http://greatfire.uchicago.edu/west-public/West/raw/master/Doc/tutorials/basic/basic_005.ipynb).\n",
    "\n",
    "Let us consider the first excitation (`# 1`) as an example: It has an energy of `0.427 eV` above the ground state and has a multiplicity of `1.081`. That means that it is a singlet, which should have a multiplicity of 1. The difference is called the *spin contamination*, describing that the spin state is note pure."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "598c8ce3",
   "metadata": {},
   "source": [
    "For a more detailed analysis, one can access the full output stored in the `solution` dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "9448b24d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['hamiltonian', 'evs_au', 'evs', 'evcs', 'rdm1s', 'mults', 'excitations']\n"
     ]
    }
   ],
   "source": [
    "print([key for key in solution.keys()])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f67aa56f",
   "metadata": {},
   "source": [
    "## Comparison between eBSE and QDET"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "767e8713",
   "metadata": {},
   "source": [
    "We can easily compare the results from eBSE and QDET as we can perform both calculations from the same output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "f277c6b7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-----------------------------------------------------\n",
      "===============================================================\n",
      "Building effective Hamiltonian...\n",
      "nspin: 2\n",
      "occupations: [[1. 1. 1. 1. 1. 1.]\n",
      " [1. 1. 1. 1. 0. 0.]]\n",
      "===============================================================\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead tr th {\n",
       "        text-align: left;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr>\n",
       "      <th></th>\n",
       "      <th colspan=\"2\" halign=\"left\"></th>\n",
       "      <th colspan=\"6\" halign=\"left\">diag[1RDM - 1RDM(GS)]</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th></th>\n",
       "      <th>E [eV]</th>\n",
       "      <th>char</th>\n",
       "      <th>87</th>\n",
       "      <th>122</th>\n",
       "      <th>123</th>\n",
       "      <th>126</th>\n",
       "      <th>127</th>\n",
       "      <th>128</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0.000</td>\n",
       "      <td>3-</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.000</td>\n",
       "      <td>0.000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0.418</td>\n",
       "      <td>1-</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.005</td>\n",
       "      <td>-0.008</td>\n",
       "      <td>-0.035</td>\n",
       "      <td>0.014</td>\n",
       "      <td>0.035</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0.418</td>\n",
       "      <td>1-</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.005</td>\n",
       "      <td>-0.008</td>\n",
       "      <td>-0.035</td>\n",
       "      <td>0.035</td>\n",
       "      <td>0.014</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>1.207</td>\n",
       "      <td>1-</td>\n",
       "      <td>-0.001</td>\n",
       "      <td>-0.010</td>\n",
       "      <td>-0.011</td>\n",
       "      <td>-0.036</td>\n",
       "      <td>0.028</td>\n",
       "      <td>0.029</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>1.835</td>\n",
       "      <td>3-</td>\n",
       "      <td>-0.001</td>\n",
       "      <td>-0.007</td>\n",
       "      <td>-0.061</td>\n",
       "      <td>-0.429</td>\n",
       "      <td>0.449</td>\n",
       "      <td>0.050</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>5</th>\n",
       "      <td>1.835</td>\n",
       "      <td>3-</td>\n",
       "      <td>-0.001</td>\n",
       "      <td>-0.007</td>\n",
       "      <td>-0.061</td>\n",
       "      <td>-0.429</td>\n",
       "      <td>0.050</td>\n",
       "      <td>0.449</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>6</th>\n",
       "      <td>2.806</td>\n",
       "      <td>1-</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.022</td>\n",
       "      <td>-0.017</td>\n",
       "      <td>-0.426</td>\n",
       "      <td>0.443</td>\n",
       "      <td>0.022</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>7</th>\n",
       "      <td>2.806</td>\n",
       "      <td>1-</td>\n",
       "      <td>-0.000</td>\n",
       "      <td>-0.022</td>\n",
       "      <td>-0.017</td>\n",
       "      <td>-0.426</td>\n",
       "      <td>0.022</td>\n",
       "      <td>0.443</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>8</th>\n",
       "      <td>4.472</td>\n",
       "      <td>1-</td>\n",
       "      <td>-0.003</td>\n",
       "      <td>-0.033</td>\n",
       "      <td>-0.082</td>\n",
       "      <td>-0.842</td>\n",
       "      <td>0.480</td>\n",
       "      <td>0.480</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9</th>\n",
       "      <td>4.896</td>\n",
       "      <td>2-</td>\n",
       "      <td>-0.008</td>\n",
       "      <td>-0.356</td>\n",
       "      <td>-0.101</td>\n",
       "      <td>-0.034</td>\n",
       "      <td>0.475</td>\n",
       "      <td>0.024</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "              diag[1RDM - 1RDM(GS)]                                 \n",
       "  E [eV] char                    87    122    123    126   127   128\n",
       "0  0.000   3-                 0.000  0.000  0.000  0.000 0.000 0.000\n",
       "1  0.418   1-                -0.000 -0.005 -0.008 -0.035 0.014 0.035\n",
       "2  0.418   1-                -0.000 -0.005 -0.008 -0.035 0.035 0.014\n",
       "3  1.207   1-                -0.001 -0.010 -0.011 -0.036 0.028 0.029\n",
       "4  1.835   3-                -0.001 -0.007 -0.061 -0.429 0.449 0.050\n",
       "5  1.835   3-                -0.001 -0.007 -0.061 -0.429 0.050 0.449\n",
       "6  2.806   1-                -0.000 -0.022 -0.017 -0.426 0.443 0.022\n",
       "7  2.806   1-                -0.000 -0.022 -0.017 -0.426 0.022 0.443\n",
       "8  4.472   1-                -0.003 -0.033 -0.082 -0.842 0.480 0.480\n",
       "9  4.896   2-                -0.008 -0.356 -0.101 -0.034 0.475 0.024"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-----------------------------------------------------\n"
     ]
    }
   ],
   "source": [
    "from westpy.qdet import QDETResult\n",
    "\n",
    "# construct object for effective Hamiltonian\n",
    "qdet = QDETResult(filename='west.wfreq.save/wfreq.json')\n",
    "\n",
    "# diagonalize Hamiltonian\n",
    "qdet_solution = qdet.solve(nelec=(5,5))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2cf75cb8",
   "metadata": {},
   "source": [
    "As we can see, both approaches yield similar energies and qualitatively the same level diagram (triplet ground state followed by 3 singlet excitations), but the spin multiplicity is different. Generally, we observe more pronounced spin contamination in eBSE than in QDET."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
