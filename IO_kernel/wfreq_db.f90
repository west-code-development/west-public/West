!
! Copyright (C) 2015-2022 M. Govoni
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
! This file is part of WEST.
!
! Contributors to this file:
! Marco Govoni
!
!-----------------------------------------------------------------------
MODULE wfreq_db
  !----------------------------------------------------------------------------
  !
  USE kinds,     ONLY : DP
  !
  IMPLICIT NONE
  !
  CONTAINS
    !
    ! *****************************
    ! WFREQ WRITE
    ! *****************************
    !
    !------------------------------------------------------------------------
    SUBROUTINE wfreq_db_write( )
      !------------------------------------------------------------------------
      !
      USE mp,                   ONLY : mp_barrier
      USE mp_world,             ONLY : mpime,root,world_comm
      USE io_global,            ONLY : stdout
      USE westcom,              ONLY : wfreq_save_dir,qp_bands,n_bands,wfreq_calculation,n_spectralf,logfile,&
                                     & sigma_exx,sigma_vxcl,sigma_vxcnl,sigma_hf,sigma_z,sigma_eqplin,&
                                     & sigma_eqpsec,sigma_sc_eks,sigma_sc_eqplin,sigma_sc_eqpsec,sigma_diff,&
                                     & sigma_freq,sigma_spectralf,l_enable_off_diagonal,pijmap,n_pairs,&
                                     & sigma_vxcl_full,sigma_vxcnl_full,sigma_exx_full,sigma_hf_full,&
                                     & sigma_sc_eks_full,sigma_sc_eqplin_full,sigma_corr_full,occupation
      USE pwcom,                ONLY : et,nspin
      USE io_push,              ONLY : io_push_bar
      USE json_module,          ONLY : json_file
      USE constants,            ONLY : rytoev
      USE types_bz_grid,        ONLY : k_grid
      !
      IMPLICIT NONE
      !
      REAL(DP),EXTERNAL :: GET_CLOCK
      REAL(DP) :: time_spent(2)
      CHARACTER(20),EXTERNAL :: human_readable_time
      INTEGER :: iks,ib,ipair
      CHARACTER(LEN=6) :: my_label_k,my_label_b
      CHARACTER(LEN=10) :: ccounter
      INTEGER :: counter
      !
      TYPE(json_file) :: json
      INTEGER :: iun,i
      INTEGER,ALLOCATABLE :: ilist(:)
      REAL(DP),ALLOCATABLE :: eks(:)
      REAL(DP),ALLOCATABLE :: occ(:)
      LOGICAL :: l_generate_plot,l_optics
      !
      ! MPI BARRIER
      !
      CALL mp_barrier(world_comm)
      !
      ! TIMING
      !
      CALL start_clock('wfreq_db')
      time_spent(1) = get_clock('wfreq_db')
      !
      IF(mpime == root) THEN
         !
         CALL json%initialize()
         !
         CALL json%load(filename=TRIM(logfile))
         !
         l_generate_plot = .FALSE.
         l_optics = .FALSE.
         DO i = 1,9
            IF(wfreq_calculation(i:i) == 'P') l_generate_plot = .TRUE.
            IF(wfreq_calculation(i:i) == 'O') l_optics = .TRUE.
         ENDDO
         !
         ALLOCATE(ilist(n_bands))
         DO ib = 1,n_bands
            ilist(ib) = qp_bands(ib)
         ENDDO
         CALL json%add('output.Q.bandmap',ilist)
         DEALLOCATE(ilist)
         !
         IF(l_enable_off_diagonal) THEN
            counter = 0
            DO ipair = 1,n_pairs
               counter = counter+1
               WRITE(ccounter,'(i10)') counter
               CALL json%add('output.Q.indexmap('//ccounter//')',(/pijmap(1,ipair),pijmap(2,ipair)/))
            ENDDO
         ELSE
            counter = 0
            DO ib = 1,n_bands
               counter = counter+1
               WRITE(ccounter,'(i10)') counter
               CALL json%add('output.Q.indexmap('//ccounter//')',(/ib,ib/))
            ENDDO
         ENDIF
         !
         IF(l_generate_plot) CALL json%add('output.P.freqlist',sigma_freq(1:n_spectralf)*rytoev)
         !
         ALLOCATE(eks(n_bands))
         ALLOCATE(occ(n_bands))
         !
         DO iks = 1,k_grid%nps
            !
            DO ib = 1,n_bands
               eks(ib) = et(qp_bands(ib),iks)
               occ(ib) = occupation(qp_bands(ib),iks)
            ENDDO
            !
            IF(nspin == 1) occ(:) = 2._DP*occ
            !
            WRITE(my_label_k,'(i6.6)') iks
            !
            CALL json%add('output.Q.K'//my_label_k//'.eks',eks*rytoev)
            CALL json%add('output.Q.K'//my_label_k//'.z',sigma_z(1:n_bands,iks))
            CALL json%add('output.Q.K'//my_label_k//'.eqpLin',sigma_eqplin(1:n_bands,iks)*rytoev)
            CALL json%add('output.Q.K'//my_label_k//'.eqpSec',sigma_eqpsec(1:n_bands,iks)*rytoev)
            CALL json%add('output.Q.K'//my_label_k//'.sigma_diff',sigma_diff(1:n_bands,iks)*rytoev)
            CALL json%add('output.Q.K'//my_label_k//'.occupation',occ)
            IF(.NOT. l_enable_off_diagonal) THEN
               CALL json%add('output.Q.K'//my_label_k//'.sigmax',sigma_exx(1:n_bands,iks)*rytoev)
               CALL json%add('output.Q.K'//my_label_k//'.vxcl',sigma_vxcl(1:n_bands,iks)*rytoev)
               CALL json%add('output.Q.K'//my_label_k//'.vxcnl',sigma_vxcnl(1:n_bands,iks)*rytoev)
               CALL json%add('output.Q.K'//my_label_k//'.hf',sigma_hf(1:n_bands,iks)*rytoev)
               CALL json%add('output.Q.K'//my_label_k//'.sigmac_eks.re',&
               & REAL(sigma_sc_eks(1:n_bands,iks)*rytoev,KIND=DP))
               CALL json%add('output.Q.K'//my_label_k//'.sigmac_eks.im',&
               & AIMAG(sigma_sc_eks(1:n_bands,iks)*rytoev))
               CALL json%add('output.Q.K'//my_label_k//'.sigmac_eqpLin.re',&
               & REAL(sigma_sc_eqplin(1:n_bands,iks)*rytoev,KIND=DP))
               CALL json%add('output.Q.K'//my_label_k//'.sigmac_eqpLin.im',&
               & AIMAG(sigma_sc_eqplin(1:n_bands,iks)*rytoev))
               CALL json%add('output.Q.K'//my_label_k//'.sigmac_eqpSec.re',&
               & REAL(sigma_sc_eqpsec(1:n_bands,iks)*rytoev,KIND=DP))
               CALL json%add('output.Q.K'//my_label_k//'.sigmac_eqpSec.im',&
               & AIMAG(sigma_sc_eqpsec(1:n_bands,iks)*rytoev))
            ELSE
               CALL json%add('output.Q.K'//my_label_k//'.sigmax',&
               & sigma_exx_full(1:n_pairs,iks)*rytoev)
               CALL json%add('output.Q.K'//my_label_k//'.vxcl',&
               & sigma_vxcl_full(1:n_pairs,iks)*rytoev)
               CALL json%add('output.Q.K'//my_label_k//'.vxcnl',&
               & sigma_vxcnl_full(1:n_pairs,iks)*rytoev)
               CALL json%add('output.Q.K'//my_label_k//'.hf',&
               & sigma_hf_full(1:n_pairs,iks)*rytoev)
               CALL json%add('output.Q.K'//my_label_k//'.sigmac_eks.re',&
               & REAL(sigma_sc_eks_full(1:n_pairs,iks)*rytoev,KIND=DP))
               CALL json%add('output.Q.K'//my_label_k//'.sigmac_eks.im',&
               & AIMAG(sigma_sc_eks_full(1:n_pairs,iks)*rytoev))
               CALL json%add('output.Q.K'//my_label_k//'.sigmac_eqpLin.re',&
               & REAL(sigma_sc_eqplin_full(1:n_pairs,iks)*rytoev,KIND=DP))
               CALL json%add('output.Q.K'//my_label_k//'.sigmac_eqpLin.im',&
               & AIMAG(sigma_sc_eqplin_full(1:n_pairs,iks)*rytoev))
               CALL json%add('output.Q.K'//my_label_k//'.sigmac_eqpSec.re',&
               & REAL(sigma_corr_full(1:n_pairs,iks)*rytoev,KIND=DP))
               CALL json%add('output.Q.K'//my_label_k//'.sigmac_eqpSec.im',&
               & AIMAG(sigma_corr_full(1:n_pairs,iks)*rytoev))
            ENDIF
            !
            IF(l_generate_plot) THEN
               DO ib = 1,n_bands
                  WRITE(my_label_b,'(i6.6)') qp_bands(ib)
                  CALL json%add('output.P.K'//my_label_k//'.B'//my_label_b//'.sigmac.re',&
                  & REAL(sigma_spectralf(1:n_spectralf,ib,iks),KIND=DP)*rytoev)
                  CALL json%add('output.P.K'//my_label_k//'.B'//my_label_b//'.sigmac.im',&
                  & AIMAG(sigma_spectralf(1:n_spectralf,ib,iks))*rytoev)
               ENDDO
            ENDIF
            !
            IF(l_optics) THEN
               CALL json%add('output.O',"optics.json")
            ENDIF
            !
         ENDDO
         !
         DEALLOCATE(eks)
         DEALLOCATE(occ)
         !
         OPEN(NEWUNIT=iun,FILE=TRIM(logfile))
         CALL json%print(iun)
         CLOSE(iun)
         CALL json%destroy()
         !
      ENDIF
      !
      ! MPI BARRIER
      !
      CALL mp_barrier(world_comm)
      !
      ! TIMING
      !
      time_spent(2) = get_clock('wfreq_db')
      CALL stop_clock('wfreq_db')
      !
      WRITE(stdout,*)
      CALL io_push_bar()
      WRITE(stdout,'(5x,"SAVE written in ",a20)') human_readable_time(time_spent(2)-time_spent(1))
      WRITE(stdout,'(5x,"In location : ",a)') TRIM(wfreq_save_dir)
      CALL io_push_bar()
      !
    END SUBROUTINE
    !
    !------------------------------------------------------------------------
    SUBROUTINE qdet_db_write_eri(eri_w,eri_vc,eri_w_full)
    !------------------------------------------------------------------------
      !
      USE mp,                   ONLY : mp_barrier
      USE mp_world,             ONLY : mpime,root,world_comm
      USE io_global,            ONLY : stdout
      USE westcom,              ONLY : wfreq_save_dir,l_enable_off_diagonal,n_pairs,logfile
      USE pwcom,                ONLY : nspin
      USE io_push,              ONLY : io_push_bar
      USE json_module,          ONLY : json_file
      USE constants,            ONLY : rytoev
      !
      IMPLICIT NONE
      !
      COMPLEX(DP),INTENT(IN):: eri_w(n_pairs,n_pairs,nspin,nspin)
      REAL(DP),INTENT(IN),OPTIONAL:: eri_vc(n_pairs,n_pairs,nspin,nspin)
      COMPLEX(DP),INTENT(IN),OPTIONAL:: eri_w_full(n_pairs,n_pairs,nspin,nspin)
      !
      REAL(DP),EXTERNAL :: GET_CLOCK
      REAL(DP) :: time_spent(2)
      CHARACTER(20),EXTERNAL :: human_readable_time
      INTEGER :: iks,jks
      CHARACTER(LEN=6) :: my_label_ik,my_label_jk,my_label_ipair
      !
      TYPE(json_file) :: json
      INTEGER :: iun,ipair
      !
      ! MPI BARRIER
      !
      CALL mp_barrier(world_comm)
      !
      ! TIMING
      !
      CALL start_clock('qdet_db')
      time_spent(1) = get_clock('qdet_db')
      !
      IF(mpime == root) THEN
         !
         CALL json%initialize()
         !
         CALL json%load(filename=TRIM(logfile))
         !
         DO iks = 1,nspin
            DO jks = 1,nspin
               !
               WRITE(my_label_ik,'(i6.6)') iks
               WRITE(my_label_jk,'(i6.6)') jks
               !
               IF(l_enable_off_diagonal) THEN
                  DO ipair = 1,n_pairs
                     !
                     WRITE(my_label_ipair,'(i6.6)') ipair
                     !
                     IF(PRESENT(eri_vc)) THEN
                        CALL json%add('qdet.eri_vc.K'//my_label_ik//'.K'//my_label_jk//'.pair'//&
                        & my_label_ipair,eri_vc(1:n_pairs,ipair,jks,iks)*rytoev)
                     ENDIF
                     !
                     IF(PRESENT(eri_w_full)) THEN
                        CALL json%add('qdet.eri_w_full.K'//my_label_ik//'.K'//my_label_jk//'.pair'//&
                        & my_label_ipair,REAL(eri_w_full(1:n_pairs,ipair,jks,iks),KIND=DP)*rytoev)
                     ENDIF
                     !
                     CALL json%add('qdet.eri_w.K'//my_label_ik//'.K'//my_label_jk//'.pair'//&
                     & my_label_ipair,REAL(eri_w(1:n_pairs,ipair,jks,iks),KIND=DP)*rytoev)
                     !
                  ENDDO
               ENDIF
               !
            ENDDO
         ENDDO
         !
         OPEN(NEWUNIT=iun,FILE=TRIM(logfile))
         CALL json%print(iun)
         CLOSE(iun)
         CALL json%destroy()
         !
      ENDIF
      !
      ! MPI BARRIER
      !
      CALL mp_barrier(world_comm)
      !
      ! TIMING
      !
      time_spent(2) = get_clock('qdet_db')
      CALL stop_clock('qdet_db')
      !
      WRITE(stdout,*)
      CALL io_push_bar()
      WRITE(stdout,'(5x "SAVE written in ",a20)') human_readable_time(time_spent(2)-time_spent(1))
      WRITE(stdout,'(5x "In location : ",a)') TRIM(wfreq_save_dir)
      CALL io_push_bar()
      !
    END SUBROUTINE
    !
    !------------------------------------------------------------------------
    SUBROUTINE qdet_db_write_h1e(h1e)
    !------------------------------------------------------------------------
      !
      USE mp,                   ONLY : mp_barrier
      USE mp_world,             ONLY : mpime,root,world_comm
      USE io_global,            ONLY : stdout
      USE westcom,              ONLY : wfreq_save_dir,l_enable_off_diagonal,n_pairs,logfile
      USE pwcom,                ONLY : nspin
      USE io_push,              ONLY : io_push_bar
      USE json_module,          ONLY : json_file
      USE constants,            ONLY : rytoev
      !
      IMPLICIT NONE
      !
      REAL(DP),INTENT(IN):: h1e(n_pairs,nspin)
      !
      REAL(DP),EXTERNAL :: GET_CLOCK
      REAL(DP) :: time_spent(2)
      CHARACTER(20),EXTERNAL :: human_readable_time
      INTEGER :: iks
      CHARACTER(LEN=6) :: my_label_ik
      !
      TYPE(json_file) :: json
      INTEGER :: iun
      !
      ! MPI BARRIER
      !
      CALL mp_barrier(world_comm)
      !
      ! TIMING
      !
      CALL start_clock('qdet_db')
      time_spent(1) = get_clock('qdet_db')
      !
      IF(mpime == root) THEN
         !
         CALL json%initialize()
         !
         CALL json%load(filename=TRIM(logfile))
         !
         DO iks = 1,nspin
            !
            WRITE(my_label_ik,'(i6.6)') iks
            !
            IF(l_enable_off_diagonal) THEN
               CALL json%add('qdet.h1e.K'//my_label_ik,h1e(1:n_pairs,iks)*rytoev)
            ENDIF
            !
         ENDDO
         !
         OPEN(NEWUNIT=iun,FILE=TRIM(logfile))
         CALL json%print(iun)
         CLOSE(iun)
         CALL json%destroy()
         !
      ENDIF
      !
      ! MPI BARRIER
      !
      CALL mp_barrier( world_comm )
      !
      ! TIMING
      !
      time_spent(2) = get_clock('qdet_db')
      CALL stop_clock('qdet_db')
      !
      WRITE(stdout,*)
      CALL io_push_bar()
      WRITE(stdout,'(5x,"SAVE written in ",a20)') human_readable_time(time_spent(2)-time_spent(1))
      WRITE(stdout,'(5x,"In location : ",a)') TRIM(wfreq_save_dir)
      CALL io_push_bar()
      !
    END SUBROUTINE
    !
END MODULE
